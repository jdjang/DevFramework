﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.User
{
    [Table("V_SUPREMA_Organization")]
    public class SPUser
    {
        /// <summary>
        /// 법인
        /// </summary>
        [Column(name: "Corp")]
        [Display(Name = "법인")]
        public string Corp { get; set; }
        /// <summary>
        /// 총괄
        /// </summary>
        [Column(name: "General")]
        [Display(Name = "총괄")]
        public string General { get; set; }
        /// <summary>
        /// 부서
        /// </summary>
        [Column(name: "Dept")]
        [Display(Name = "부서")]
        public string Dept { get; set; }
        /// <summary>
        /// 팀
        /// </summary>
        [Column(name: "Team")]
        [Display(Name = "팀")]
        public string Team { get; set; }        
        /// <summary>
        /// 소속(최종 소속)
        /// </summary>
        [Column(name: "Ad_Department")]
        [Display(Name = "소속")]
        public string AdDepartment { get; set; }
        /// <summary>
        /// 그룹코드(부서/팀 코드)
        /// </summary>
        [Column(name: "GroupCode")]
        [Display(Name = "그룹코드")]
        public string GroupCode { get; set; }
        /// <summary>
        /// 사번
        /// </summary>
        [Column(name: "Sabun")]
        [Display(Name = "사번")]
        public string Sabun { get; set; }
        /// <summary>
        /// 이름
        /// </summary>
        [Column(name: "DisplayName")]
        [Display(Name = "이름")]
        public string DisplayName { get; set; }
        /// <summary>
        /// 직책
        /// </summary>
        [Column(name: "JobTitle")]
        [Display(Name = "직책")]
        public string JobTitle { get; set; }
        /// <summary>
        /// 직급
        /// </summary>
        [Column(name: "JobGrade")]
        [Display(Name = "직급")]
        public string JobGrade { get; set; }
        /// <summary>
        /// 상위 그룹 코드
        /// </summary>
        [Column(name: "MemberOf")]
        [Display(Name = "상위 그룹 코드")]
        public string MemberOf { get; set; }
        /// <summary>
        /// 메일 주소
        /// </summary>
        [Column(name: "Email")]
        [Display(Name = "메일 주소")]
        public string EmailAddress { get; set; }
        /// <summary>
        /// 사진 경로(URL)
        /// </summary>
        [Column(name: "PhotoPath")]
        [Display(Name = "사진 경로")]
        public string PhotoPath { get; set; }
        /// <summary>
        /// 입사일
        /// </summary>
        [Column(name: "EnterDate")]
        [Display(Name = "입사일")]
        public string EnterDate { get; set; }
        /// <summary>
        /// 퇴사일
        /// </summary>
        [Column(name: "RetireDate")]
        [Display(Name = "퇴사일")]
        public string RetireDate { get; set; }
        /// <summary>
        /// 법인정렬
        /// </summary>
        public Int16 CorpSort { get; set; }
        /// <summary>
        /// 부서 정렬
        /// </summary>
        public int DeptSort { get; set; }
        /// <summary>
        /// 직급 정렬
        /// </summary>
        public int JobLevelSort { get; set; }
        /// <summary>
        /// 사용자 정렬
        /// </summary>
        public int UserSort { get; set; }
        /// <summary>
        /// 겸직 여부
        /// </summary>
        public bool IsAddJob { get; set; }        
    }
}
