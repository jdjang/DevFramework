﻿using PagedList.Core.Mvc;

namespace WebCore.Pagination
{
    public class SitePagedListRenderOptions
    {
        public static PagedListRenderOptions Bootstrap4Full
        {
            get
            {
                var option = PagedListRenderOptions.Bootstrap4Full;
                option.LinkToFirstPageFormat = "처음";
                option.LinkToLastPageFormat = "마지막";
                option.LinkToPreviousPageFormat = "이전";
                option.LinkToNextPageFormat = "다음";
                option.MaximumPageNumbersToDisplay = 5;
                return option;
            }
        }
    }
}
