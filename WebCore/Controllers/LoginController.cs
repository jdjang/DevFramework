﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WebCore.Data;

namespace WebCore.Controllers
{
    public class LoginController : Controller
    {
        private readonly Covi_Flow_SI_Context _context;        
        public LoginController(Covi_Flow_SI_Context context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail(string sabun)
        {
            if (string.IsNullOrEmpty(sabun))
            {
                return NotFound();
            }

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Sabun == sabun);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }
    }
}
