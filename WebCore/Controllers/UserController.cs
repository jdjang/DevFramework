﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;
using System.Linq;
using System.Threading.Tasks;
using WebCore.Data;

namespace WebCore.Controllers
{
    public class UserController : Controller
    {
        private readonly Covi_Flow_SI_Context _context;

        public UserController(Covi_Flow_SI_Context context)
        {
            _context = context;
        }
        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : string.Empty;

            if (!string.IsNullOrEmpty(searchString))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"]= searchString;

            var users = from s in _context.Users
                        select s;
            if (!string.IsNullOrEmpty(searchString))
            {
                users = users.Where(u => u.DisplayName.Contains(searchString));
            }
            users.OrderBy(u => u.Sabun);

            int pageSize = 10;
            return View(users.ToPagedList(page ?? 1, pageSize));            
        }

        public async Task<IActionResult> Detail(string sabun, int pageIndex)
        {
            if (string.IsNullOrEmpty(sabun))
            {
                return NotFound();
            }

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Sabun == sabun);

            if (user == null)
            {
                return NotFound();
            }
            ViewData["PageIndex"] = pageIndex;

            return View(user);
        }
    }
}
