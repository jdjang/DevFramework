﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCore.Models.User;

namespace WebCore.Data
{
    public class Covi_Flow_SI_Context : DbContext
    {
        public Covi_Flow_SI_Context(DbContextOptions<Covi_Flow_SI_Context> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SPUser>().HasNoKey();
        }

        public DbSet<SPUser> Users { get; set; }
    }
}
